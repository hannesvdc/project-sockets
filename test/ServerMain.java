package test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServerMain {

	public static void main(String[] args) {
		while(true){
			try {
				DatagramSocket socket = new DatagramSocket(1302);
				byte[] buf = new byte[100];
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				socket.receive(packet);
				System.out.println(new String(packet.getData()));
				
				InetAddress ad = packet.getAddress();
				System.out.println("Sending message in broadcast");
				buf = new byte[]{1,2,3,4,5,6,7,8,9};
				System.out.println("port "+packet.getPort());
				DatagramPacket packet2 = new DatagramPacket(buf,  buf.length, ad, packet.getPort());
				socket.send(packet2);
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
