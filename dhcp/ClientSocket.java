package dhcp;

import java.net.SocketException; 

/**
 * This class is a specific class for the client, because timeouts are needed.
 * 
 * @author Hannes Vandecasteele
 *
 */
public class ClientSocket extends DHCPSocket {

	public ClientSocket(int port) throws SocketException {
		super(port);
		setSoTimeout(10000);
	}
	
	

}
