package dhcp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * This class is a subclass of a DatagramSocket
 * to send messages over UDP. It will take Messages
 * as input and output.
 * 
 * @author Hannes Vandecasteele
 * 
 */
public class DHCPSocket extends DatagramSocket{
	
	/**
	 * Override constructor of DatagramSocket. The port argument is 
	 * the port this socket is bound to, not the port of the server.
	 * See Message.port for details.
	 * 
	 * A timeout of ten seconds is also set.
	 * 
	 * @throws SocketException
	 */
	public DHCPSocket(int port) throws SocketException {
		super(port);
	}
	
	/**
	 * Send a Message using this socket.
	 * 
	 * @throws IOException when the submission of the message does not complete
	 * 		   correctly.
	 */
	public void send(Message message) throws IOException{
		byte [] bytes = message.toBytes();
		InetAddress host = message.getHost();
		int toport = message.getPort();
		
		DatagramPacket packet = new DatagramPacket(bytes, bytes.length, host, toport);
		this.send(packet);
	}
	
	/**
	 * This method receives a Message from the host. This method returns true
	 * when a message is accepted, false otherwise.
	 * @throws SocketTimeoutException 
	 */
	public boolean receive( Message message ) {
		try{
			DatagramPacket packet = new DatagramPacket(new byte[1000], 1000);
			this.receive(packet);
			message.readBytes(packet.getData());			
			message.setHost(packet.getAddress());
			message.setPort(packet.getPort());
			return true;
		} catch( IOException e ){
			System.out.println("No message was received.");
			return false;
		}
		
	}
}
