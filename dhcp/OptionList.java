package dhcp;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

/**
 * This class represents the list of options
 * that can be sent in a DHCP message. It also has
 * an inner class which represents a particular option.
 * This is necessary because options can be of variable 
 * size.
 * 
 * @author Hannes Vandecasteele
 *
 */
public class OptionList {

	
	/**
	 * This inner class represents a specific option in the list.
	 * 
	 * @author Hannes Vandecasteele
	 *
	 */
	class Option{
		private byte code;
		private byte len;
		private byte[] content;
		
		/**
		 * Constructor with all parameters.
		 */
		public Option( byte c, byte l, byte[] co ){
			code = c;
			len = l;
			content = co;
		}
	}
	
	/**
	 * The following constants define a known list of options
	 */
	public static final byte MESSAGE_TYPE = 53;
	public static final byte LEASETIME    = 51;
	public static final byte REQUEST_IP   = 50;
	public static final byte SERVERNAME   = 66;
	public static final byte SERVERID     = 54;
	
	
	/**
	 * The variables to maintain the list of options.
	 * A hash table is used because this makes it easier
	 * to update existing options, add new ones and 
	 * remove existing ones.
	 */
	private Hashtable<Byte, Option> options;
	
	/**
	 * Default constructor.
	 */
	public OptionList(){
		options = new Hashtable<Byte, Option>();
	}
	
	/**
	 * Add an option to the list if it not already exists.
	 * If so, its value and length is changed.
	 */
	public void addOption(byte code, byte[] value){
		Option o = new Option(code, (byte)value.length, value);
		options.put(code, o);
	}
	
	/**
	 * Get the value of a given option.
	 */
	public byte[] getOption(byte code){
		Option o = options.get(code);
		if( o == null ){
			return null;
		}
		return o.content;
	}
	
	/**
	 * Remove an option from the list.
	 */
	public void removeOption(byte code){
		options.remove(code);
	}
	
	/**
	 * Check if the option exists.
	 */
	public boolean contains(byte code){
		return options.containsKey(code);
	}
	
	/**
	 * Convert the list of options to a representation in bytes.
	 * Remember that only options 0 and 255 are of fixed 
	 * length. But these represent either an empty option, or
	 * the end of the list. Therefore, they should not be considered.
	 * 
	 * The specifications dictates that the list of options has to 
	 * be 312 bytes long.
	 */
	public byte[] toBytes(){
		byte[] bytes = new byte[312];
		int pos = 4;
		Set<Byte> keys= options.keySet();
		Iterator<Byte> iterator = keys.iterator();
		bytes[0] = 99;
		bytes[1] = (byte) 130;
		bytes[2] = 83;
		bytes[3] = 99;
				
		// If there is a message type option, this should
		// come first.
		if( keys.contains(MESSAGE_TYPE) ){
			bytes[pos] = MESSAGE_TYPE;
			Option o = options.get(MESSAGE_TYPE);
			bytes[pos+1] = o.len;
			pos += 2;
			
			for( int i = 0 ; i < o.len ; ++i ){
				bytes[pos] = o.content[i];
				pos++;
			}
		}
		
		
		while( iterator.hasNext() ){
			Byte code = iterator.next();
			Option o = options.get(code);
			
			// We have already included the message type, 
			// so we don't need to do it again.
			if( code == MESSAGE_TYPE ) continue;
			
			bytes[pos] = code;
			pos++;
			bytes[pos] = o.len;
			pos++;
			
			for( int i = 0 ; i < o.len ; ++i ){
				bytes[pos] = o.content[i];
				pos++;
			}
		}
		
		// This indicates the end of the list of options.
		bytes[pos] = (byte) 255;
		
		return bytes;
	}
	
	/**
	 * This method converts the given array of bytes to a correct list of
	 * options.
	 * @throws IOException 
	 */
	public void toOptions(DataInputStream bytes ) throws IOException{		
		for( int i = 0 ; i < 4 ; ++i ){
			bytes.readByte();
		}
		
		while( true ){
			byte code = bytes.readByte();
			
			//end of the options reached
			if( code == (byte)255 ) return;
			
			byte len = bytes.readByte();
			
			byte[] value = new byte[len];
			for( int i = 0 ; i < len ; ++i ){
				value[i] = bytes.readByte();
			}
			
			// finally, add the calculated option.
			addOption(code, value);
		}
	}
	
	/**
	 * This method will print the list of options.
	 */
	public void printOptions(){
		Set<Byte> keys= options.keySet();
		Iterator<Byte> iterator = keys.iterator();

		while( iterator.hasNext() ){
			Byte c = iterator.next();
			Option o = options.get(c);
			System.out.print( "code "+o.code+ " len "+o.len + " value ");
			byte [] array = o.content;
			for( int i = 0 ; i < array.length ; ++i ){
				System.out.print(array[i]+" ");
			}
			System.out.println();
		}
	}
}
