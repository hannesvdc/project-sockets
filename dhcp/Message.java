package dhcp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 * This class represents a DHCP message that can be sent over
 * the DHCP connection. It has all the fields as described in the
 * message format.
 * 
 * @author Hannes Vandecasteele
 *
 */
public class Message {

	/**
	 * The following variables represent the internal 
	 * structure of a DHCP message. All the fields are
	 * present.
	 */
	private byte opcode;
	private byte htype;
	private byte hlength;
	private byte hcount;
	private int id;
	private short secs;
	private short flags;
	private byte[] ciaddr = new byte[4];
	private byte[] yiaddr = new byte[4];
	private byte[] siaddr = new byte[4];
	private byte[] giaddr = new byte[4];
	private byte[] chaddr = new byte[16];
	private byte[] sname  = new byte[64];
	private byte[] file   = new byte[128];
	private OptionList options;	// contains all the options
	
	/**
	 * This field contains the IP address to which this
	 * message is to be sent or from whom this is received.
	 */
	private InetAddress address;
	
	/**
	 * This field contains the port the message should be sent to.
	 */
	private int port;
	
	/**
	 * A list of special messages such as DISCOVER, OFFER, REQUEST, ACK, NACK and DECLINE.
	 */
	public static final byte DISCOVER = 1;
	public static final byte OFFER = 2;
	public static final byte REQUEST = 3;
	public static final byte DECLINE = 4;
	public static final byte ACK = 5;
	public static final byte NACK = 6;
	public static final byte RELEASE = 7;
	public static final byte INFORM = 8;
	
	/**
	 * A list of constants for the opcode.
	 */
	public static final byte REQUESTCODE = 1;
	public static final byte REPLYCODE = 2;
	
	/**
	 * Constructor for this message. A destination can be given.
	 */
	public Message(InetAddress host, int port){
		super();
		this.address = host;
		this.port = port;
		options = new OptionList();
	}
	
	/**
	 * Default constructor for this message. The IP address will be
	 * null. The port is 0.
	 */
	public Message(){
		this(null, 0);
	}
	/**
	 * The next few method are getters and setters for the private properties.
	 */
	public byte getOpcode(){
		return opcode;
	}
	
	public void setOpcode(byte c){
		opcode = c;
	}
	
	public byte getHardwareType(){
		return htype;
	}
	
	public void setHardwareType(byte h ){
		htype = h;
	}
	
	public byte getHLength(){
		return hlength;
	}
	
	public void setHLength(byte l ){
		hlength = l;
	}
	
	public byte getHopCount(){
		return hcount;
	}
	
	public void setHopCount(byte h){
		hcount = h;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId( int i ){
		id = i;
	}
	
	public short getSecs(){
		return secs;
	}
	
	public void setSecs(short s ){
		secs = s;
	}
	
	public short getFlags(){
		return flags;
	}
	
	public void setFlags(short f ){
		flags = f;
	}
	
	public byte[] getSiaddr(){
		return siaddr;
	}
	
	public void setSiadrr(byte a[] ){
		siaddr = a;
	}
	
	public byte[] getYiaddr(){
		return yiaddr;
	}
	
	public void setYiadrr(byte a[] ){
		yiaddr = a;
	}
	
	public byte[] getGiaddr(){
		return giaddr;
	}
	
	public void setGiadrr(byte a[] ){
		giaddr = a;
	}
	
	public byte[] getCiaddr(){
		return ciaddr;
	}
	
	public void setCiadrr(byte a[] ){
		ciaddr = a;
	}
	
	public byte[] getChaddr(){
		return chaddr;
	}
	
	public void setChadrr(byte a[] ){
		chaddr = a;
	}
	
	public byte[] getSname(){
		return sname;
	}
	
	public void setSname(byte a[] ){
		sname = a;
	}
	
	public byte[] getFile(){
		return file;
	}
	
	public void setFile(byte a[] ){
		file = a;
	}
	
	public OptionList getOptions(){
		return options;
	}
	
	public void setOptions(OptionList l ){
		options = l;
	}
	
	public void addOption(byte code, byte[] content ){
		options.addOption(code, content);
	}
	
	public byte[] getOption(byte code ){
		return options.getOption(code);
	}
	
	/**
	 * Get and set the host to send to.
	 */
	public InetAddress getHost(){
		return address;
	}
	
	public void setHost( InetAddress a ){
		address = a;
	}
	
	/**
	 * Get and set the port to send to.
	 */
	public int getPort(){
		return port;
	}
	
	public void setPort(int p ){
		port = p;
	}
	
	/**
	 * This method converts the message to an array
	 * of bytes which can be sent over the UDP connection.
	 * 
	 * @return a list of bytes representing this message.
	 */
	public byte[] toBytes() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(stream);
		
		try{
			out.writeByte(opcode);
			out.writeByte(htype);
			out.writeByte(hlength);
			out.writeByte(hcount);
			out.writeInt(id);
			out.writeShort(secs);
			out.writeShort(flags);
			out.write(ciaddr);
			out.write(yiaddr);
			out.write(siaddr);
			out.write(giaddr);
			out.write(chaddr);
			out.write(sname);
			out.write(file);
			/*System.out.println("In converting options to bytes");
			System.out.println(bytesToHex(options.toBytes()));*/
			out.write(options.toBytes());
			/*System.out.println("Size");
			System.out.println(out.size());*/
			out.flush();
		} catch( IOException e ){
			System.out.println("An error has occured while converting the message to a byte array.");
		}
	
		return stream.toByteArray();
	}
	
	/**
	 * This method converts the given array of bytes to a 
	 * DHCP message with fields and options.
	 */
	public void readBytes(DataInputStream stream){
		try{
			opcode = stream.readByte();
			htype = stream.readByte();
			hlength = stream.readByte();
			hcount = stream.readByte();
			id = stream.readInt();
			secs = stream.readShort();
			flags = stream.readShort();
			stream.readFully(ciaddr);
			stream.readFully(yiaddr);
			stream.readFully(siaddr);
			stream.readFully(giaddr);
			stream.readFully(chaddr);
			stream.readFully(sname);
			stream.readFully(file);
			options.toOptions(stream);
		} catch( IOException e ){
			System.out.println("An error has occured while interpreting the bytes.");
		}
	}
	
	/**
	 * This method converts a given array of bytes to a dhcp message.
	 */
	public void readBytes(byte[] array){
		ByteArrayInputStream bstream = new ByteArrayInputStream(array, 0, array.length );
		DataInputStream stream = new DataInputStream (bstream);
		this.readBytes(stream);
	}
	
	/**
	 * This method will print the Message to the standard output.
	 */
	public void print(){
		System.out.println("Opcode "+getOpcode());
		System.out.println("Htype "+getHardwareType());
		System.out.println("HLength "+getHLength());
		System.out.println("Hcount "+getHopCount());
		System.out.println("Id "+getId());
		System.out.println("Secs "+getSecs());
		System.out.println("Flags "+getFlags());
		System.out.println("Ciaddr "+bToString(getCiaddr()));
		System.out.println("Yiaddr "+bToString(getYiaddr()));
		System.out.println("Siaddr "+bToString(getSiaddr()));
		System.out.println("Giaddr "+bToString(getGiaddr()));
		System.out.println("Chaddr "+bToString(getChaddr()));
		System.out.println("Sname "+bToString(getSname()));
		System.out.println("File "+bToString(getFile()));
		System.out.println("Options");
		this.options.printOptions();
	}
	
	/**
	 * This method will print the message in hexadecimals.
	 * Each byte is represented by two hexadecimal numbers.
	 */
	public void printBytes(){
		byte[] data = toBytes();
		System.out.println("Starting message");
		System.out.println(bytesToHex(data));
	}
	
	/**
	 * This private method will convert an array of bytes to the corresponding strings.
	 */
	private String bToString( byte[] bytes ){
		String result = "";
		for( int i = 0 ; i < bytes.length ; ++i ){
			result = result + bytes[i] + " ";
		}
		
		return result;
	}
	
	/**
	 * Static method to convert an array of bytes to a String of 
	 * hexadecimals. Useful for debugging.
	 */
	protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2+bytes.length];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j*3] = ' ';
	        hexChars[j * 3+1] = hexArray[v >>> 4];
	        hexChars[j * 3 + 2] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
}
