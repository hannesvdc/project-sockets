package dhcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Random;


/**
 * This class represents the DHCP client. It is an extension of a
 * Thread because it controls the leasing of the IP address completely
 * autonomously.
 * 
 * @author Hannes Vandecasteele
 *
 */
public class Client extends Thread{

	/**
	 * We will use one socket to manage all the dhcp 
	 * requests. This avoids endlessly creating and 
	 * dumpint sockets.
	 */
	private ClientSocket socket;
	
	/**
	 * A field that contains the IP address of the server
	 * to send to. This field will contain the broadcasting
	 * address in the beginning.
	 */
	private InetAddress server;
	
	/**
	 * This field contains the port on which the server listens.
	 */
	private int serverPort = 1234; //1302;//1234;
	
	/**
	 * This field contains the hardware address of this pc.
	 */
	private byte[] macaddress;
	
	/**
	 * This field will contain the IP address this client can use.
	 * It is only a dummy variable since it has no physical meaning
	 * in this assignment.
	 */
	private byte[] clientAddress;
	
	/**
	 * This variable indicates if the client should continue to 
	 * lease an ip address, or release the address.
	 */
	private boolean released = false;
	
	/**
	 * the lease time
	 */
	private int leaseseconds;
	
	/**
	 * The parameters T1 and T2 that are used for the leasetime.
	 */
	private int T1, T2;
	
	/**
	 * This counter represents the number of times the client will reconnect.
	 */
	private int iterations = 0;
	
	/**
	 * Default constructor. Creates a new socket object.
	 * The server IP is set to the broadcast IP.
	 * 
	 * @throws SocketException 
	 */
	public Client(int port, byte m) throws SocketException{
		socket = new ClientSocket(port);
		
		// First, get the IP of the server. This is the broadcast address.
		try {
			server = InetAddress.getByName("192.168.43.233");//"localhost");//"10.33.14.246");
		} catch (UnknownHostException e) {
			System.out.println("Cannot bind to the server.");
		}
		
		// Then, find the hardware address of this device.
		try {
			macaddress = new byte[]{127, 127, 127, 127, 127, 127,127, 127, 127, 127, 127, 127,127, 127, 127, m};
			
		} catch (Exception e) {
			System.out.println("Cannot read the mac address of this computer.");
		}
	}
	
	/**
	 * This method will send a discover message to the server as 
	 * a broadcast, and will return the response from the server.
	 * 
	 * @return The response Message object from the server. This can 
	 * 		   be null if the discovery did not succeed.
	 */
	private Message discover(){
		// First, create the message used to discover.
		// and fill in all the necessary fields.
		Message discoverMessage = createDiscoverMessage();
		
		// and then send the message
		try {
			System.out.println("\n\nSending the discover message.");
			discoverMessage.print();
			socket.send(discoverMessage);
		} catch (IOException e) {
			System.out.println("Problems while sending the discover message.");
			return null;
		}
		
		// Loop until the server responds with a message with the 
		// same id.
		while( true ){
			Message in = new Message();
			
			// If the server does not respond, try later.
			boolean result = socket.receive(in);
			if( !result ) return null;			
			
			// check if the id's match
			if( in.getId() == discoverMessage.getId() && in.getOption(OptionList.MESSAGE_TYPE)[0] == Message.OFFER ){
				System.out.println("\n\nDiscover response received.");
				in.print();
				
				return in;
			}
		}
	}
	
	/**
	 * This method will send discovermessages until the server reponds.
	 * This is the init state as in the RFC document.
	 * @throws SocketException 
	 */
	private Message discoverState() throws SocketException{
		socket.setSoTimeout(10000);
		iterations = 0;
		
		while( true ){
			Message m = discover();
			if( m != null ) return m;
		}
	}
	
	
	/**
	 * This method answers to an offer from the server by sending a request.
	 * The message is broadcast to the same address as the discover. See
	 * official guidelines.
	 */
	private Message makeRequest(Message response){
		// Create the message and set the fields. Most of the fields
		// are the same, so we copy it.
		System.out.println("\n\nmaking request");
		Message sendmessage = createRequestMessage(response);
		
		// and send the request.
		try {
			System.out.println("Sending request message");
			sendmessage.print();
			socket.send(sendmessage);
		} catch ( Exception e ){
			System.out.println("Problems with sending the request message.");
			return null;
		}
		
		// Now, wait for an answer from the server.
		while( true ){
			Message serverResponse = new Message();
			boolean result = socket.receive(serverResponse);
			
			// If the receive times out, retry later.
			if( !result ) return null;
				
			// check if the id's match.
			if( serverResponse.getId() == response.getId()
				&&  ( serverResponse.getOption(OptionList.MESSAGE_TYPE)[0] == Message.ACK || 
						serverResponse.getOption(OptionList.MESSAGE_TYPE)[0] == Message.NACK )){
				System.out.println("\n\nReceived ACK or NAK from the server.");
				serverResponse.print();
				return serverResponse;
			}
		}
	}
	
	/**
	 * This method will resend request until four iterations. 
	 * If, after that, the server does not respond, the client
	 * will go in discover state.
	 * 
	 * @throws SocketException 
	 */
	private Message requestState(Message m) throws SocketException{
		int it = 0;
		while( it < 4 ){
			Message out = makeRequest(m);
			if( out != null ) return out;
			it++;
		}
		
		return discoverState();
	}
	/**
	 * This method will renew an existing ip address.
	 */
	private Message renew(){
		// Create the renew message. The specification says
		// that we cannot fill in the servers name, nor the 
		// requested ip address.
		Message renewmessage = createRenewMessage();
		
		try {
			System.out.println("\n\nRenewing IP");
			renewmessage.print();
			socket.send(renewmessage);
		} catch( Exception e ){
			System.out.println("Problems in renewing lease.");
			return null;
		}	
		
		// And wait for the answer of the server
		while( true ){
			Message inmessage = new Message();
			boolean result = socket.receive(inmessage);
			
			// If we do not get a result due to a timeout, return.
			if( !result ) return null;
			
			// check if id's match, then return the result.
			if( inmessage.getId() == renewmessage.getId() ){
				System.out.println("\n\nRenew response received from server");
				inmessage.print();
				return inmessage;
			}
		}
	}
	
	/**
	 * This method will send a renew message four times before
	 * restarting the initialization procedure. If the server still
	 * does not respond, the client will go into rebind state.
	 */
	private Message renewState(){
		try {
			socket.setSoTimeout(1000*(T2-T1));
			Message out = renew();
			
			if( out == null){
				return rebindState();
			}

			return out;
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * This method represents the rebindstate if the server has not
	 * yet responded in T2 seconds. If there is a timeout, the server will 
	 * go into discover state.
	 */
	private Message rebindState() {
		try {
			socket.setSoTimeout(1000*leaseseconds/8);
			Message out = renew();
			
			if( out == null ){
				return discoverState();
			}
			
			return out;
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		return null;		
	}
	/**
	 * This method will release the IP address of the client and inform the server.
	 * The connection socket will also be closed.
	 */
	private void releaseAddress(Message response){
		// Create the release message.
		Message message = createReleaseMessage(response);
		
		// and send in to the server.
		System.out.println("\n\nReleasing our IP address.");
		message.print();
		try {
			socket.send(message);
		} catch (IOException e) {
			System.out.println("Problems in releaseing IP.");
		}
		
		// and, of course, close the socket connection.
		socket.close();
	}
	
	/**
	 * This method is the entry point for the client thread. It will create a connection
	 * and renew it when the lease time is less than half of the original time.
	 */
	@Override
	public void run(){
		Message response = null;
		
		try{
			// In the beginning, we will send a discover message.
			System.out.println("Starting discovering the DHCP server.");
			response = discoverState();

			// Now, we will determine what the type of the message is.
			while( !released ){
				byte messagetype = response.getOption(OptionList.MESSAGE_TYPE)[0];
				
				// We will switch over the message type because there are a few types.
				switch( messagetype ){
				
				// The server responded with a possible IP offer. We will send a request.
				case Message.OFFER:
					System.out.println("Received an offer for ip " + Message.bytesToHex(response.getYiaddr()) );
					ByteBuffer lease = ByteBuffer.wrap(response.getOption(OptionList.LEASETIME));
					leaseseconds = lease.getInt();
					T1 = leaseseconds/2;
					T2 = 7*leaseseconds/8;
					response = requestState(response);
					
					break;
					
				// The server acknowledges our request. This means we can set
				// our IP, change the server IP and sleep until half of the 
				// the lease time is over.
				case Message.ACK:
					iterations++;
					System.out.println("The server grants the IP address.");
					clientAddress = response.getYiaddr();
					server = InetAddress.getByAddress(response.getSiaddr());
					
					// and sleep for half the lease time
					Thread.sleep(Math.min(1000 * T1, 10000) );

					// And then renew the lease.
					if( iterations == 3 ){
						release();
						break;
					}
					
					// After the wait, renew.
					response = renewState();
					
					break;
					
				// The server does not acknowledge our request, so we have
				// to send a new discover message.
				case Message.NACK:
					System.out.println("Received NAK from the server, restart discovering.");
					
					// We have to wait at least ten seconds before rediscovering
					Thread.sleep(10000);
					response = discoverState();
					break;
				}
			}
		
		} catch (Exception e ){
			System.out.println("An unexpected exception occurred in the client. Abort.");
			return;
		}
		
		// And finally, release the ip address.
		releaseAddress(response);
	}
	
	/**
	 * This method should be called when the ip address is to be released.
	 */
	public void release(){
		released = true;
	}
	
	/**
	 * This method creates a discovery Message object.
	 */
	private Message createDiscoverMessage(){
		Message discoverMessage = new Message(server, serverPort);
		Random r = new Random();
		int id = Math.abs(r.nextInt());
		discoverMessage.setOpcode(Message.REQUESTCODE);	   // request an ip
		discoverMessage.setHardwareType((byte) 1); // Internet
		discoverMessage.setHLength((byte) 6);	   // six bytes in the MAC address
		discoverMessage.setHopCount((byte) 0);	  // no hops passed
		discoverMessage.setId(id);
		discoverMessage.setSecs((short) 0);
		discoverMessage.setFlags((short) 0);
		discoverMessage.setChadrr(macaddress);
		discoverMessage.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.DISCOVER} );
		
		return discoverMessage;
	}
	
	/**
	 * This method creates a request message, based on the offer of the server.
	 */
	private Message createRequestMessage(Message response){
		Message sendmessage = new Message(server, serverPort);
		sendmessage.setId(response.getId());
		sendmessage.setOpcode(Message.REQUESTCODE);
		sendmessage.setHardwareType((byte) 1); // Internet
		sendmessage.setHLength((byte) 6);	  // six bytes in the MAC address
		sendmessage.setChadrr(macaddress);
		sendmessage.setHopCount((byte) 0);	  // no hops passed
		sendmessage.setSecs((short) 0);
		sendmessage.setFlags((short) 0);
		sendmessage.setSname(response.getSname());		// also send the server identifier.
		sendmessage.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.REQUEST} );
		sendmessage.addOption(OptionList.SERVERID, response.getOption(OptionList.SERVERID)); 
		sendmessage.addOption(OptionList.REQUEST_IP, response.getYiaddr());
		
		return sendmessage;
	}
	
	/**
	 * This method creates a renew message to request the current ip from the server.
	 */
	private Message createRenewMessage(){
		Random r = new Random();
		int id = r.nextInt();
		Message renewmessage = new Message(server, serverPort);
		renewmessage.setId(id);
		renewmessage.setOpcode(Message.REQUESTCODE);
		renewmessage.setChadrr(macaddress);
		renewmessage.setHardwareType((byte)1); // Internet
		renewmessage.setHLength((byte)6);	   // six bytes in the MAC address
		renewmessage.setHopCount((byte) 0);	  // no hops passed
		renewmessage.setSecs((short) 0);
		renewmessage.setFlags((short) 0);
		renewmessage.setCiadrr(clientAddress);
		renewmessage.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.REQUEST});
		
		return renewmessage;
	}
	
	/**
	 * This method creates a release message to the server.
	 */
	private Message createReleaseMessage(Message response){
		Message message = new Message(server, serverPort);
		Random r = new Random();
		int id = r.nextInt();
		message.setId(id);
		message.setOpcode(Message.REQUESTCODE);
		message.setChadrr(macaddress);
		message.setHardwareType((byte)1); // Internet
		message.setHLength((byte)6);	   // six bytes in the MAC address
		message.setHopCount((byte) 0);	  // no hops passed
		message.setSecs((short) 0);
		message.setFlags((short) 0);
		message.setCiadrr(clientAddress);
		message.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.RELEASE});
		message.addOption(OptionList.SERVERID, response.getOption(OptionList.SERVERID));
		
		return message;
	}
}
