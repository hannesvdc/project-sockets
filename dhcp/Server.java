package dhcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * This class is the DHCP server that will contain a pool
 * of possible IP addresses.
 * 
 * @author Hannes Vandecasteele
 *
 */
public class Server {

	/**
	 * This is the socket that will start accepting
	 * connections from clients.
	 */
	private DHCPSocket socket;
	
	/**
	 * A list of possible IP addresses
	 */
	private ArrayList<IP> ips;
	
	/**
	 * This variable contains the IP address of this server.
	 */
	private InetAddress thisip;
	
	/**
	 * Default constructor.
	 * 
	 * @throws SocketException 
	 * @throws UnknownHostException 
	 */
	public Server() throws SocketException, UnknownHostException{
		socket = new DHCPSocket(1302);
		ips = new ArrayList<IP>();
		ips.add(new IP(new byte[]{4,5,6,7}));
		ips.add(new IP(new byte[]{8,9,10,11}));
		ips.add(new IP(new byte[]{12,13,14,15}));
		thisip = InetAddress.getLocalHost(); 
		new Receiver().run();
	}
	
	
	/**
	 * This inner class represents an IP address. It can be 
	 * assigned to a client. In this case, the mac address should
	 * be stored.
	 */
	private class IP {
		private byte[] address;
		private byte[] mac;
		private boolean inuse;
		private Timer timer;		// a timer to make a timout if the client does not 
									// renew its lease in time
		
		/**
		 * Default constructor that instantiates an IP.
		 * 
		 * @param address
		 */
		public IP( byte[] address ){
			timer = new Timer();
			this.address = address;
			this.mac = null;
			this.inuse = false;
		}
		
		/**
		 * Method to claim an ip with a mac address
		 */
		public void setMac( byte[] mac ){
			this.mac = mac;
		}
		
		/**
		 * Get the macaddress
		 */
		public byte[] getMac(){
			return mac;
		}
		
		/**
		 * Set the IP in used state
		 */
		public void setUsed(boolean u){
			inuse = u;
		}
		
		/**
		 * Getter of use.
		 */
		public boolean isUsed(){
			return inuse;
		}
		
		/**
		 * Return the IP
		 */
		public byte[] getIP(){
			return address;
		}
		
		/**
		 * Initiates a timer.
		 */
		public void setTimer(int lease){
			timer.cancel();
			timer = new Timer();
			timer.schedule(new TimerTask(){
			
				@Override
				public void run(){
					cancelIP(mac);
				}
				
				
			}, 1000*lease);
		}
		
		/**
		 * Cancel the running timer.
		 */
		public void cancelTimer(){
			timer.cancel();
		}
		
		/**
		 * Print the current state of this IP address.
		 */
		public void print(){
			System.out.print("IP "+bytesToString(getIP()));
			if( inuse ){
				System.out.print(" used by client "+ bytesToString(getMac()) + "\n");
			} else {
				System.out.print(" Not in use.\n");
			}
		}
	}
	
	
	/**
	 * This inner class is responsible for receiving incoming
	 * requests from the clients.
	 */
	private class Receiver extends Thread {
		
		/**
		 * Only one method: accept incoming connections and dispatch
		 * these to the correct handler.
		 */
		@Override
		public void run() {
			try {
				while( true ){
					Message incoming = new Message();
					socket.receive(incoming);
				
					// If the incoming message is no request, let is pass
					if( incoming.getOpcode() != Message.REQUESTCODE ) continue;
					byte type = incoming.getOption(OptionList.MESSAGE_TYPE)[0];
				
					// Switch over the possible message types and dispatch
					switch( type ){
					case Message.DISCOVER:
						dispatchDiscover(incoming);
						break;
					case Message.REQUEST:
						dispatchRequest(incoming);
						break;
					case Message.RELEASE:
						dispatchRelease(incoming);
						break;
					}
				}
			}catch ( Exception e ){
				System.out.println("Error in server.");
			}
		}
		
		/**
		 * Method that launches a discover thread.
		 */
		private void dispatchDiscover(final Message in){
			new Thread(){
				public void run(){
					discoverMessage(in);
				}
			}.start();
		}
		
		/**
		 * Method that launches a request thread.
		 */
		private void dispatchRequest(final Message in){
			new Thread(){
				public void run(){
					requestMessage(in);
				}
			}.start();
		}
		
		/**
		 * Method that launches a release thread.
		 */
		private void dispatchRelease(final Message in){
			new Thread(){
				public void run(){
					releaseMessage(in);
				}
			}.start();
		}
	}
	
	
	/**
	 * This function receives an incoming discover message.
	 * It should allocate an ip address for this client and 
	 * forward an offer message.
	 * 
	 * @param message
	 * @throws IOException 
	 */
	private void discoverMessage(Message message){
		System.out.println("\n\nReceived a discover message from a client.");
		
		// First, we find an used IP address.
		IP ip = null;
		for( int i = 0 ; i < ips.size() ; ++i ){
			if( ips.get(i).getMac() == null ){
				ip = ips.get(i);
			}
		}
		
		if( ip == null ){
			System.out.println("Ran out of possible IP addresses.");
			return;
		}
		
		// Update the ip address logic
		ip.setMac(message.getChaddr());
		System.out.println("Client mac ");
		printBytes(message.getChaddr());
		
		// Now, we send an offer message.
		Message out = createOfferMessage(message, ip);
		
		try {
			socket.send(out);
			System.out.println("Offering IP to client ");
			printBytes(ip.getIP());
		} catch (IOException e) {
			System.out.println("Could not send an offer message to the client.");
		}
		
		printPool();
	}
	
	/**
	 * This function should handle an incoming request message.
	 * This can either be after a discover message, or after 
	 * a renew.
	 */
	private void requestMessage(Message message){
		System.out.println("\n\nThe client wants to either request its ip or extend it.");
		
		byte[] mac = message.getChaddr();
		IP ip = getIP(mac);
		System.out.println("Client mac ");
		printBytes(mac);
		
		if( ip == null ){
			System.out.println("Could not find IP associated with the given mac address.");
			sendNak(message);
			return;
		}
		
		// Create the ACK message te send.
		Message out = createAckMessage(message, ip);
		
		// and finally, send the ACK message.
		try {
			socket.send(out);
			System.out.println("The server grants this ip address.");
			ip.setUsed(true);
		} catch (IOException e) {
			System.out.println("Could not send ACK message. Abort and let the client timeout");
		}
		
		ip.setTimer(10);
		printPool();
	}
	
	/**
	 * This function should be called when an ip address can be
	 * released.
	 * @throws  
	 */
	private void releaseMessage(Message message) {
		System.out.println("\n\nA client wishes to end its lease of an ip.");
		
		byte[] mac = message.getChaddr();
		IP ip = getIP(mac);
		
		if( ip == null || !ip.isUsed() ){
			System.out.println("There is a problem with the IP address associated with the mac. Abort.");
			return;
		}
		
		System.out.println("Client mac ");
		printBytes(mac);
		
		// Make sure the ip address can be reused.
		ip.setUsed(false);
		ip.setMac(null);
		ip.cancelTimer();
		
		// and send the ack message
		Message out = createReleaseMessage(message);
		try {
			socket.send(out);
			System.out.println("The server acknowledges the release of the IP address.");
		} catch (IOException e) {
			System.out.println("Could not release IP");
		}
		
		printPool();
	}
	
	/**
	 * This method will send a NAK message to the given host and port.
	 */
	private void sendNak(Message message ){
		Message out = new Message(message.getHost(), message.getPort());
		out.setHardwareType(Message.REPLYCODE); // Internet
		out.setHLength((byte) 6);	   // six bytes in the MAC address
		out.setHopCount((byte) 0);	  // no hops passed
		out.setId(message.getId());
		out.setSecs(message.getSecs());
		out.setFlags(message.getFlags());
		out.setChadrr(message.getChaddr());
		out.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.NACK} );

		try {
			socket.send(out);
			System.out.println("The server does not grant the IP address.");
		} catch (IOException e) {
			System.out.println("Could not send NAK message. Abort.");
		}
	}
	
	/**
	 * Cancel an IP if the timer has expired.
	 */
	private void cancelIP(byte[] mac){
		System.out.println("\n\nCancelling IP for client");
		IP ip = getIP(mac);
		ip.setMac(null);
		ip.setUsed(false);
		System.out.println("Client mac");
		printBytes(mac);
		
		printPool();
	}
	
	/**
	 * Print the current state of the pool of IP addresses.
	 */
	private void printPool(){
		System.out.println("\n IP Pool: ");
		for( int i = 0 ; i < ips.size(); ++i ){
			IP ip = ips.get(i);
			ip.print();
		}
	}
	
	/**
	 * This method returns an IP address associated with the mac address.
	 */
	private IP getIP(byte[] mac ){
		for( int i=0;i < ips.size() ; ++i ){
			boolean equal = true;
			byte m[] = ips.get(i).getMac();
			
			if( m == null ) continue;
			
			for( int j = 0 ; j < 16 ; ++j ){
				if( mac[j] != m[j] ){
					equal = false;
					break;
				}
			}
			
			if( equal ) return ips.get(i);
		}
		
		return null;
	}
	
	/**
	 * Create an offer message based on the received discover and the ip address.
	 */
	private Message createOfferMessage(Message message, IP ip ){
		Message out = new Message(message.getHost(), message.getPort());
		out.setOpcode(Message.REPLYCODE);	   // response
		out.setHardwareType((byte) 1); // Internet
		out.setHLength((byte) 6);	   // six bytes in the MAC address
		out.setHopCount((byte) 0);	   // no hops passed
		out.setId(message.getId());
		out.setSecs(message.getSecs());
		out.setFlags(message.getFlags());
		out.setChadrr(message.getChaddr());
		out.setYiadrr(ip.getIP());
		out.setSiadrr(thisip.getAddress());
		out.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.OFFER} );
		out.addOption(OptionList.SERVERID, thisip.getAddress());
		out.addOption(OptionList.LEASETIME, new byte[]{0,0,0,10});
		
		return out;
	}
	
	/**
	 * Create an ACK message to respond to a request, based on
	 * the received message and the ip.
	 */
	private Message createAckMessage(Message message, IP ip ){
		// Now, we only have to send an ACK message
		Message out = new Message(message.getHost(), message.getPort());
		out.setOpcode(Message.REPLYCODE);
		out.setHardwareType((byte) 1); // Internet
		out.setHLength((byte) 6);	   // six bytes in the MAC address
		out.setHopCount((byte) 0);	  // no hops passed
		out.setId(message.getId());
		out.setSecs(message.getSecs());
		out.setFlags(message.getFlags());
		out.setChadrr(message.getChaddr());
		out.setSiadrr(thisip.getAddress());
		out.setYiadrr(ip.getIP());
		out.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.ACK} );
		out.addOption(OptionList.SERVERID, thisip.getAddress());
		out.addOption(OptionList.LEASETIME, new byte[]{0,0,0,10});
		return out;
	}
	
	/**
	 * Create a release message based on a release request.
	 */
	private Message createReleaseMessage(Message message ){
		Message out = new Message(message.getHost(), message.getPort());
		out.setOpcode(Message.REPLYCODE);
		out.setHardwareType((byte) 1); // Internet
		out.setHLength((byte) 6);	   // six bytes in the MAC address
		out.setHopCount((byte) 0);	  // no hops passed
		out.setId(message.getId());
		out.setSecs(message.getSecs());
		out.setFlags(message.getFlags());
		out.setChadrr(message.getChaddr());
		out.setSiadrr(thisip.getAddress());
		out.addOption(OptionList.MESSAGE_TYPE, new byte[]{Message.ACK} );

		return out;
	}
	
	
	private void printBytes( byte[] b ){
		System.out.println(bytesToString(b));
	}
	
	private String bytesToString(byte[] b ){
		String str = "";
		for( int i = 0 ; i < b.length ; ++i ){
			str = str + b[i] + " ";
		}
		
		return str;
	}
	
}
